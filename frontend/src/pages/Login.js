import { useContext, useEffect, useState } from 'react';
import { Button, Container, Form } from 'react-bootstrap';
import { Helmet } from 'react-helmet-async';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getError } from '../error';
import { Store } from '../Store';

export default function Login() {
  const navigate = useNavigate();
  const { search } = useLocation();
  // from the search object inside useLocation we're gonna redirect in the URL from URL search params and passing the search object and get the 'redirect' in the query string
  const redirectInUrl = new URLSearchParams(search).get('redirect');
  // the value in 'redirectInUrl' is = /shipping
  const redirect = redirectInUrl ? redirectInUrl : '/';
  // check redirectInUrl if exist set in in the redirect and if not it will go to the default redirect which is the home page

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const { state, dispatch: ctxDispatch } = useContext(Store);
  const { userInfo } = state;

  const submitHandler = async e => {
    e.preventDefault();
    try {
      const { data } = await fetch('/api/users/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: email,
          password: password,
        }),
      });
      //console.log(data);
      ctxDispatch({ type: 'USER_LOGIN', payload: data });
      localStorage.setItem('userInfo', JSON.stringify(data));
      navigate(redirect || '/');
    } catch (err) {
      toast.error(getError(err));
    }
  };

  // if user already login it will not to go the /login page
  useEffect(() => {
    if (userInfo) {
      navigate(redirect);
    }
  }, [navigate, redirect, userInfo]);

  return (
    <Container className="small-container">
      <Helmet>
        <title>Login</title>
      </Helmet>
      <h1 className="my-3">Login</h1>
      <Form onSubmit={submitHandler}>
        <Form.Group className="mb-3" controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="email"
            placeholder="Email Address"
            onChange={e => setEmail(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            onChange={e => setPassword(e.target.value)}
            required
          />
        </Form.Group>
        <div className="mb-3">
          <Button type="submit" className="login-btn">
            Login
          </Button>
        </div>
        <div className="mb-3">
          New Customer?{' '}
          <Link to={`/register?redirect=${redirect}`}>Create you account</Link>
        </div>
      </Form>
    </Container>
  );
}
