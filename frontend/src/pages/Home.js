import { useEffect, useReducer, useState } from 'react';
import logger from 'use-reducer-logger';
import { Helmet } from 'react-helmet-async';
import { Col, Row } from 'react-bootstrap';
import ProductList from '../components/ProductList';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import Banner from '../components/Banner';

// import data from '../data';

const reducer = (state, action) => {
  switch (action.type) {
    case 'FETCH_REQUEST':
      return { ...state, loading: true };
    case 'FETCH_SUCCESS':
      return { ...state, products: action.payload, loading: false };
    case 'FETCH_FAIL':
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};

export default function Home() {
  const [{ loading, error, products }, dispatch] = useReducer(logger(reducer), {
    products: [],
    loading: true,
    error: '',
  });

  // const [products, setProducts] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      dispatch({ type: 'FETCH_REQUEST' });
      try {
        const result = await fetch('/api/products');
        dispatch({ type: 'FETCH_SUCCESS', payload: result.data });
      } catch (err) {
        dispatch({ type: 'FETCH_FAIL', payload: err.message });
      }

      // setProducts(result.data);
    };
    fetchData();
  }, []);

  return (
    <div className="mt-5">
      <Helmet>
        <title>CarmajSwittut</title>
      </Helmet>
      <Banner />
      <h1 className="mt-5 mb-4">Featured Products</h1>
      <div className="products">
        {loading ? (
          <LoadingBox />
        ) : error ? (
          <MessageBox variant="danger">{error}</MessageBox>
        ) : (
          <Row>
            {products.map(product => (
              <Col key={product.slug} sm={6} m={4} lg={3} className="mb-3">
                <ProductList product={product}></ProductList>
              </Col>
            ))}
          </Row>
        )}
      </div>
    </div>
  );
}
