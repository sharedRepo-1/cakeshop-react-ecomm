import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { Store } from '../Store';
import Checkout from '../components/Checkout';

export default function ShippingAddress() {
  const navigate = useNavigate();

  const { state, dispatch: ctxDispatch } = useContext(Store);

  const {
    userInfo,
    cart: { shippingAddress },
  } = state;

  const [fullName, setFullName] = useState(shippingAddress.fullName || '');
  const [address, setAddress] = useState(shippingAddress.address || '');
  const [city, setCity] = useState(shippingAddress.city || '');
  const [barangay, setBarangay] = useState(shippingAddress.barangay || '');
  const [deliveryTime, setDeliveryTime] = useState(
    shippingAddress.deliveryTime || ''
  );

  useEffect(() => {
    if (!userInfo) {
      navigate('/login?redirect=/shipping');
    }
  }, [userInfo, navigate]);

  const submitHandler = e => {
    e.preventDefault();

    ctxDispatch({
      type: 'SAVE_SHIPPING_ADDRESS',
      payload: {
        fullName,
        address,
        city,
        barangay,
        deliveryTime,
      },
    });
    localStorage.setItem(
      'shippingAddress',
      JSON.stringify({ fullName, address, city, barangay, deliveryTime })
    );
    navigate('/payment');
  };
  return (
    <div>
      <Helmet>
        <title>Shipping Address</title>
      </Helmet>

      <Checkout step1 step2></Checkout>
      <div className="container small-container my-5">
        <h1 className="my-3 text-center">Shipping Address</h1>
        <Form onSubmit={submitHandler}>
          <Form.Group className="mb-3" controlId="fullName">
            <Form.Label>Full Name</Form.Label>
            <Form.Control
              value={fullName}
              onChange={e => setFullName(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="address">
            <Form.Label>Address: Unit / House No., Building, Street</Form.Label>
            <Form.Control
              value={address}
              onChange={e => setAddress(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="city">
            <Form.Label>City</Form.Label>
            <Form.Control
              value={city}
              onChange={e => setCity(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="barangay">
            <Form.Label>Barangay</Form.Label>
            <Form.Control
              value={barangay}
              onChange={e => setBarangay(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="deliveryTime">
            <Form.Label className="mb-3">Delivery Time</Form.Label>
            <Form.Select
              value={deliveryTime}
              onChange={e => setDeliveryTime(e.target.value)}
            >
              <option>Select Available Delivery Time</option>
              <option value="12:00:00">12:00 PM</option>
              <option value="13:00:00">01:00 PM</option>
              <option value="14:00:00">02:00 PM</option>
              <option value="15:00:00">03:00 PM</option>
              <option value="16:00:00">04:00 PM</option>
              <option value="17:00:00">05:00 PM</option>
              <option value="18:00:00">06:00 PM</option>
            </Form.Select>
            {/* <Form.Control
            value={deliveryTime}
            onChange={e => setdeliveryTime(e.target.value)}
          /> */}
          </Form.Group>
          <div className="mb-3">
            <Button type="submit" className="shipping-btn">
              Continue
            </Button>
          </div>
        </Form>
      </div>
    </div>
  );
}
