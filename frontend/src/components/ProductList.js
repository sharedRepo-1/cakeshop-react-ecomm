import { useContext } from 'react';
import { Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Store } from '../Store';
import Rating from './Rating';

export default function ProductList(props) {
  const { product } = props;

  const { state, dispatch: ctxDispatch } = useContext(Store);
  const {
    cart: { cartItems },
  } = state;

  //checker if item exists or not
  const addToCartHandler = async item => {
    const existItem = cartItems.find(x => x._id === product._id);
    const quantity = existItem ? existItem.quantity + 1 : 1;
    const { data } = await fetch(`/api/products/${item._id}`);

    if (data.countInStock < quantity) {
      Swal.fire({
        title: 'Sorry, Product is out of Stock!',
        icon: 'error',
      });
      return;
    }
    // dispatch the action and for the payload pass the product
    ctxDispatch({
      type: 'CART_ADD_ITEM',
      payload: { ...item, quantity },
    });
  };

  return (
    <Card>
      <Link to={`/products/${product.slug}`}>
        <img
          src={product.image}
          className="card-img-top p-3"
          alt={product.name}
        />
      </Link>
      <Card.Body>
        <Link to={`/products/${product.slug}`}>
          <Card.Title>{product.name}</Card.Title>
        </Link>
        <Rating rating={product.rating} numReviews={product.numReviews} />
        <Card.Text>₱ {product.price}</Card.Text>
        {product.countInStock === 0 ? (
          <Button variant="light" disabled>
            Out of stock
          </Button>
        ) : (
          <Button
            className="add-cart-btn"
            onClick={() => addToCartHandler(product)}
          >
            Add to cart
          </Button>
        )}
      </Card.Body>
    </Card>
  );
}
