import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';

export default function Banner() {
  return (
    <Container className="small-container">
      <Row className="d-none d-sm-block">
        <Col>
          <img src="./images/banner.png" alt="banner"></img>
        </Col>
      </Row>
    </Container>
  );
}
