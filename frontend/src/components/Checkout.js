import React from 'react';
import { Col, Row } from 'react-bootstrap';

export default function Checkout(props) {
  return (
    <Row className="checkout-steps mx-2">
      <Col className={props.step1 ? 'active' : ''}>Login</Col>
      <Col className={props.step2 ? 'active' : ''}>Shipping</Col>
      <Col className={props.step3 ? 'active' : ''}>Payment</Col>
      <Col className={props.step4 ? 'active' : ''}>Place Order</Col>
    </Row>
  );
}
