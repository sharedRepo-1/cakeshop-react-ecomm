import express from 'express';
import Product from '../models/productModel.js';

// import data from '../data.js';

const productRouter = express.Router();

// Retrieve all products
productRouter.get('/', async (req, res) => {
  const products = await Product.find();
  res.send(products);
});

// Retrive product by slug
productRouter.get('/slug/:slug', async (req, res) => {
  const product = await Product.findOne({ slug: req.params.slug });

  if (product) {
    res.send(product);
  } else {
    res.status(404).send({ message: 'Product Not Found!' });
  }
});

// Retrive product by Id
productRouter.get('/:id', async (req, res) => {
  const product = await Product.findById(req.params.id);

  if (product) {
    res.send(product);
  } else {
    res.status(404).send({ message: 'Product Not Found!' });
  }
});

export default productRouter;
